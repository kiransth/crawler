<?php
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
    $urls = array("http://en.wikipedia.org/wiki/FIFA_15");
    $parsed = array();

    $sitesvisited = 0;

    @mysql_connect("localhost", "root", "");
    @mysql_select_db("crawler");

    function parse_site() {
        GLOBAL $urls, $parsed, $sitesvisited;

        $newsite = array_shift($urls);

        echo "\n Now parsing $newsite...\n";

        // the @ is because not all URLs are valid, and don't want
        // lots of errors being printed out
        $ourtext = @file_get_contents($newsite);
        if (!$ourtext) return;

        $newsite = addslashes($newsite);
        $ourtext = addslashes($ourtext);



        // this site has been successfully indexed; increment the counter
        ++$sitesvisited;

        // this extracts all hyperlinks in the document
        preg_match_all("/http:\/\/[A-Z0-9_\-\.\/\?\#\=\&]*/i", $ourtext, $matches);

        if (count($matches)) {
            $matches = $matches[0];
            $nummatches = count($matches);

            echo "Got $nummatches from $newsite\n";

            foreach($matches as $match) {

                // ignoring all these strings
                if (stripos($match, ".exe") !== false) continue;
                if (stripos($match, ".zip") !== false) continue;
                if (stripos($match, ".rar") !== false) continue;
                if (stripos($match, ".wmv") !== false) continue;
                if (stripos($match, ".wav") !== false) continue;
                if (stripos($match, ".mp3") !== false) continue;
                if (stripos($match, ".sit") !== false) continue;
                if (stripos($match, ".mov") !== false) continue;
                if (stripos($match, ".avi") !== false) continue;
                if (stripos($match, ".msi") !== false) continue;
                if (stripos($match, ".rpm") !== false) continue;
                if (stripos($match, ".rm") !== false) continue;
                if (stripos($match, ".ram") !== false) continue;
                if (stripos($match, ".asf") !== false) continue;
                if (stripos($match, ".mpg") !== false) continue;
                if (stripos($match, ".mpeg") !== false) continue;
                if (stripos($match, ".tar") !== false) continue;
                if (stripos($match, ".tgz") !== false) continue;
                if (stripos($match, ".bz2") !== false) continue;
                if (stripos($match, ".deb") !== false) continue;
                if (stripos($match, ".pdf") !== false) continue;
                if (stripos($match, ".jpg") !== false) continue;
                if (stripos($match, ".jpeg") !== false) continue;
                if (stripos($match, ".gif") !== false) continue;
                if (stripos($match, ".tif") !== false) continue;
                if (stripos($match, ".png") !== false) continue;
                if (stripos($match, ".swf") !== false) continue;
                if (stripos($match, ".svg") !== false) continue;
                if (stripos($match, ".bmp") !== false) continue;
                if (stripos($match, ".dtd") !== false) continue;
                if (stripos($match, ".xml") !== false) continue;
                if (stripos($match, ".js") !== false) continue;
                if (stripos($match, ".vbs") !== false) continue;
                if (stripos($match, ".css") !== false) continue;
                if (stripos($match, ".ico") !== false) continue;
                if (stripos($match, ".rss") !== false) continue;
                if (stripos($match, "w3.org") !== false) continue;    

                                                                        // yes, these next two are very vague, but they do cut out
                                                                        // the vast majority of advertising links. 
                if (stripos($match, "ads.") !== false) continue;
                if (stripos($match, "ad.") !== false) continue;

                if (stripos($match, "doubleclick") !== false) continue;

                                                                                    // this URL looks safe
                if (!in_array($match, $parsed)) 
                {                                                                   // we haven't already parsed this URL...
                    if (!in_array($match, $urls)) 
                    {  
                        array_push($urls, $match);
                        mysql_query("INSERT INTO `crawler`.`crawler_data` (`index`, `URL`) VALUES (NULL, '".$match."');");
                        echo "<br/>".$match;
                    }
                }
            }
        } 
        else 
        {
            echo "Got no matches from $newsite\n";
        }

        // add this site to the list we've visited already
        $parsed[] = $newsite;
    }

    while ($sitesvisited < 1000 && count($urls) != 0) 
    {
        parse_site();

        // this stops us from overloading web servers
        sleep(50);
    }
?>